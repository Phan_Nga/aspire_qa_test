# Aspire_QA_Test
1.Install required software and libraries below:
- 1.1 Eclipse
- 1.2 Java JDK 1.8
- 1.3 Selenium selenium-server-standalone-3.141.59
- 1.4 testng-6.9.10

2.Execution via Eclipse
- 2.1 Go to folder "src/test/resource/suites"
- 2.2 Select file run.xml -> right click -> Run As -> TestNG suite

3.The test automation framework is comprised of following tools and libraries
-  TestNG: Framework
-  Page Object Model
-  Selenium: Browser automation tool
-  JAVA: Programming language
-  Eclipse: Integrated Development Environment
-  Git-Gitlab: Version Control, Git repository hosted server
-  ReportNG-report-plugin: Reporting


