package commons;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {

	public By byXpath(String xpathValue) {
		return By.xpath(xpathValue);
	}

	public String getDynamicLocator(String xpathValue, String... values) {
		xpathValue = String.format(xpathValue, (Object[]) values);
		return xpathValue;
	}

	public void sendKeyToElement(WebDriver driver, String xpathValue, String value) {
		element = driver.findElement(byXpath(xpathValue));
		element.clear();
		element.sendKeys(value);
	}

	public void sendKeyToElement(WebDriver driver, String xpathValue, String value, String... values) {
		element = driver.findElement(byXpath(getDynamicLocator(xpathValue, values)));
		element.clear();
		element.sendKeys(value);
	}

	public void waitElementVisible(WebDriver driver, String xpathValue) {
		explicitWait = new WebDriverWait(driver, longTimeout);
		explicitWait.until(ExpectedConditions.visibilityOfElementLocated(byXpath(xpathValue)));
	}

	public void waitElementVisible(WebDriver driver, String xpathValue, String... values) {
		explicitWait = new WebDriverWait(driver, longTimeout);
		explicitWait.until(ExpectedConditions.visibilityOfElementLocated(byXpath(getDynamicLocator(xpathValue, values))));
	}

	public WebElement find(WebDriver driver, String xpathValue) {
		return driver.findElement(byXpath(xpathValue));
	}

	public void checkToCheckbox(WebDriver driver, String xpathValue) {
		element = find(driver, xpathValue);
		if (!element.isSelected()) {
			element.click();
		}
	}

	public void checkToCheckbox(WebDriver driver, String xpathValue, String... values) {
		element = find(driver, getDynamicLocator(xpathValue, values));
		if (!element.isSelected()) {
			element.click();
		}
	}

	public void waitElementClickable(WebDriver driver, String xpathValue) {
		explicitWait = new WebDriverWait(driver, longTimeout);
		explicitWait.until(ExpectedConditions.elementToBeClickable(byXpath(xpathValue)));
	}

	public void waitElementClickable(WebDriver driver, String xpathValue, String... values) {
		explicitWait = new WebDriverWait(driver, longTimeout);
		explicitWait.until(ExpectedConditions.elementToBeClickable(byXpath(getDynamicLocator(xpathValue, values))));
	}

	public void clickToElement(WebDriver driver, String xpathValue) {
		driver.findElement(byXpath(xpathValue)).click();
	}

	public List<WebElement> finds(WebDriver driver, String xpathValue) {
		return driver.findElements(byXpath(xpathValue));
	}

	public void selectItemInCustomDropdown(WebDriver driver, String parentElement, String childElement, String expected) {
		jsExecutor = (JavascriptExecutor) driver;
		clickToElement(driver, parentElement);
		explicitWait = new WebDriverWait(driver, longTimeout);
		explicitWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byXpath(childElement)));
		elements = finds(driver, childElement);
		for (WebElement item : elements) {
			if (item.getText().trim().equals(expected)) {
				jsExecutor.executeScript("arguments[0].scrollIntoView(true);", item);
				item.click();
				break;
			}
		}

	}

	private WebElement element;
	private List<WebElement> elements;
	private JavascriptExecutor jsExecutor;
	private WebDriverWait explicitWait;
	private long longTimeout = 30;
}
