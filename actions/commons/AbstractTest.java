package commons;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class AbstractTest {
	WebDriver driver;
	protected WebDriver getBrowserDriver(String browserName, String url) {
		BrowserName browser = BrowserName.valueOf(browserName.toUpperCase());		
		if (browser == BrowserName.CHROME) {
			System.setProperty("webdriver.chrome.driver","./browserDrivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser == BrowserName.FIREFOX) {
			System.setProperty("webdriver.firefox.driver","./browserDrivers/chromedriver.exe");
			driver = new FirefoxDriver();
		} else if (browser == BrowserName.EDGE) {
			System.setProperty("webdriver.edge.driver","./browserDrivers/chromedriver.exe");
			driver = new EdgeDriver();
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(url);
		return driver;
	}
}
