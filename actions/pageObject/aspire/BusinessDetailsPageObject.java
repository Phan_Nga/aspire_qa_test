package pageObject.aspire;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.aspire.BusinessDetailsPageUI;

public class BusinessDetailsPageObject extends AbstractPage {
	private WebDriver driver;

	public BusinessDetailsPageObject(WebDriver driver) {
		this.driver = driver;
	}

	public void clickToGetStartedButton() {
		waitElementClickable(driver, BusinessDetailsPageUI.GET_STARTED_BUTTON);
		clickToElement(driver, BusinessDetailsPageUI.GET_STARTED_BUTTON);

	}

	public void inputToBusinessName(String bussinessName) {
		waitElementVisible(driver, BusinessDetailsPageUI.BUSINESS_NAME);
		sendKeyToElement(driver, BusinessDetailsPageUI.BUSINESS_NAME, bussinessName);
	}

	public void selectRegistrationType(String registrationType) {
		selectItemInCustomDropdown(driver, BusinessDetailsPageUI.PARENT_REGISTRATION_TYPE, BusinessDetailsPageUI.CHILD_REGISTRATION_TYPE,registrationType );
		
	}

	public void selectIndustry(String industry) {
		selectItemInCustomDropdown(driver, BusinessDetailsPageUI.PARENT_INDUSTRY, BusinessDetailsPageUI.CHILD_INDUSTRY, industry );
		
	}

	public void selectSubIndustry(String sub_industry) {
		selectItemInCustomDropdown(driver, BusinessDetailsPageUI.PARENT_SUB_INDUSTRY, BusinessDetailsPageUI.CHILD_SUB_INDUSTRY, sub_industry );
		
	}

	public void inputUEN(String UEN) {
		waitElementVisible(driver, BusinessDetailsPageUI.UEN);
		sendKeyToElement(driver, BusinessDetailsPageUI.UEN, UEN);
		
	}

	public void clickToSubmitButton() {
		waitElementClickable(driver, BusinessDetailsPageUI.SUBMIT_BUTTON);
		clickToElement(driver, BusinessDetailsPageUI.SUBMIT_BUTTON);
	}

}
