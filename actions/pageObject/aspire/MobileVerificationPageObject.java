package pageObject.aspire;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.aspire.MobileVerificationPageUI;

public class MobileVerificationPageObject extends AbstractPage {
	private WebDriver driver;

	public MobileVerificationPageObject(WebDriver driver) {
		this.driver = driver;
	}

	public void inputOTPNumber(String OTPnumber) {
		waitElementVisible(driver, MobileVerificationPageUI.OTP_NUMBER);
		sendKeyToElement(driver, MobileVerificationPageUI.OTP_NUMBER, OTPnumber);
	}

}
