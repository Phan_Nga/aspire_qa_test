package pageObject.aspire;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.aspire.OnboardingPageUI;

public class OnboardingPageObject extends AbstractPage {
	private WebDriver driver;

	public OnboardingPageObject(WebDriver driver) {
		this.driver = driver;
	}

	public void clickToContinuesButton() {
		waitElementClickable(driver, OnboardingPageUI.CONTINUES_BUTTON);
		clickToElement(driver, OnboardingPageUI.CONTINUES_BUTTON);
	}

	public void clickToContinuesButtonACRA() {
		waitElementClickable(driver, OnboardingPageUI.CONTINUES_ACRA_BUTTON);
		clickToElement(driver, OnboardingPageUI.CONTINUES_ACRA_BUTTON);
		
	}

	public void clickToGetStartedButton() {
		waitElementClickable(driver, OnboardingPageUI.GET_STARTED_BUTTON);
		clickToElement(driver, OnboardingPageUI.GET_STARTED_BUTTON);
		
	}
}
