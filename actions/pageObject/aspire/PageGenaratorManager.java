package pageObject.aspire;

import org.openqa.selenium.WebDriver;

public class PageGenaratorManager {
	public static RegisterPageObject getRegisterPage(WebDriver driver) {
		return new RegisterPageObject(driver);
	}

	public static MobileVerificationPageObject getMobileVerificationPage(WebDriver driver) {
		return new MobileVerificationPageObject(driver);
	}

	public static OnboardingPageObject getOnboardingPage(WebDriver driver) {
		return new OnboardingPageObject(driver);
	}

	public static PersonalDetailsObject getPersonalDetailsPage(WebDriver driver) {
		return new PersonalDetailsObject(driver);
	}
	public static BusinessDetailsPageObject getBussinessDetailsPage(WebDriver driver) {
		return new BusinessDetailsPageObject(driver);
	}
}
