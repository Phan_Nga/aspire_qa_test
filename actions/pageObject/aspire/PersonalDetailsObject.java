package pageObject.aspire;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.aspire.PersonalDetailsPageUI;

public class PersonalDetailsObject extends AbstractPage {
	private WebDriver driver;

	public PersonalDetailsObject(WebDriver driver) {
		this.driver = driver;
	}

	public void clickGetStartedButton() {
		waitElementClickable(driver, PersonalDetailsPageUI.GET_STARTED_BUTTON);
		clickToElement(driver, PersonalDetailsPageUI.GET_STARTED_BUTTON);
	}

	public void sendKeyToDate() {
		waitElementVisible(driver, PersonalDetailsPageUI.DATE_OF_BIRTH);
		sendKeyToElement(driver, PersonalDetailsPageUI.DATE_OF_BIRTH, "Jan 4, 1996");
	}

	public void selectNationality(String nationality) {
			selectItemInCustomDropdown(driver, PersonalDetailsPageUI.PARENT_NATIONAL, PersonalDetailsPageUI.CHILD_NATIONAL, nationality);

		}

	public void selectGender(String gender) {
		selectItemInCustomDropdown(driver, PersonalDetailsPageUI.PARENT_GENDER, PersonalDetailsPageUI.CHILD_GENDER, gender);		
	}

	public void selectProduct(String product) {
		selectItemInCustomDropdown(driver, PersonalDetailsPageUI.PARENT_PRODUCTS, PersonalDetailsPageUI.CHILD_PRODUCTS, product);		
		
	}

	public void clickToSubmitButton() {
		waitElementClickable(driver, PersonalDetailsPageUI.SUBMIT_BUTTON);
		clickToElement(driver, PersonalDetailsPageUI.SUBMIT_BUTTON);
	}
}
