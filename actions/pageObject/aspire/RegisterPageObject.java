package pageObject.aspire;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import commons.AbstractPage;
import pageUIs.aspire.RegisterPageUI;

public class RegisterPageObject extends AbstractPage {
	private WebDriver driver;

	public RegisterPageObject(WebDriver driver) {
		this.driver = driver;
	}

	public void clickToRegisterButton() {
		waitElementClickable(driver, RegisterPageUI.REGISTER_BUTTON);
		clickToElement(driver, RegisterPageUI.REGISTER_BUTTON);
	}

	public void inputIntoTextboxName(WebDriver driver, String textboxName, String value) {
		waitElementVisible(driver, RegisterPageUI.DYNAMIC_TEXTBOX_NAME, textboxName);
		sendKeyToElement(driver, RegisterPageUI.DYNAMIC_TEXTBOX_NAME, value, textboxName);
	}

	public void selectRole(String role) {
		waitElementClickable(driver, RegisterPageUI.ROLE, role);
		checkToCheckbox(driver, RegisterPageUI.ROLE, role);
	}

	public void selectWhere(String expected) {
		selectItemInCustomDropdown(driver, RegisterPageUI.PARENT, RegisterPageUI.CHILD, expected);

	}

	public void inputIntoPromoCode(String value) {
		waitElementVisible(driver, RegisterPageUI.PROMODE_CODE);
		sendKeyToElement(driver, RegisterPageUI.PROMODE_CODE, value);

	}

	public void agreeTermsAndConditions() {
		waitElementVisible(driver, RegisterPageUI.TERMS_POLICY);
		checkToCheckbox(driver, RegisterPageUI.TERMS_POLICY);

	}

	public void clickToContinueButton() {
		waitElementClickable(driver, RegisterPageUI.CONTINUE_BUTTON);
		clickToElement(driver, RegisterPageUI.CONTINUE_BUTTON);
	}

	public void inputOTPNumber() {
		// waitElementVisible(driver, RegisterPageUI.OTP_NUMBER);
		List<WebElement> otpList = driver.findElements(By.cssSelector("div[class*='digit-input__box'] div"));
		for (int i = 0; i < otpList.size(); i++) {
			int value = i + 1;
			switch (value) {
			case 1:
				sendKeyToElement(driver, RegisterPageUI.OTP_NUMBER, "1");
			case 2:
				sendKeyToElement(driver, RegisterPageUI.OTP_NUMBER, "2");
			case 3:
				sendKeyToElement(driver, RegisterPageUI.OTP_NUMBER, "3");
			case 4:
				sendKeyToElement(driver, RegisterPageUI.OTP_NUMBER, "4");
			}
		}
	}
}
