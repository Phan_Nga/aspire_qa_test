package pageUIs.aspire;

public class BusinessDetailsPageUI {
	public static final String GET_STARTED_BUTTON = "//span[text()='Get Started']";
	public static final String BUSINESS_NAME = "//input[@autocomplete='organization']";
	public static final String PARENT_REGISTRATION_TYPE = "//div[contains(text(),'Registration Type')]/parent::div/following-sibling::label//i";
	public static final String CHILD_REGISTRATION_TYPE = "//div[@class='q-item__label']";
	public static final String UEN = "//input[@placeholder='Business Registration Number UEN']";
	public static final String PARENT_INDUSTRY = "//div[contains(text(),'Industry')]/parent::div/following-sibling::label//i";
	public static final String CHILD_INDUSTRY = "//div[@class='q-item__label']";
	public static final String PARENT_SUB_INDUSTRY = "//div[contains(text(),'Sub Industry')]/parent::div/following-sibling::label//i";
	public static final String CHILD_SUB_INDUSTRY = "//div[@class='q-item__label']";
	public static final String SUBMIT_BUTTON = "//span[text()='Submit]";
}