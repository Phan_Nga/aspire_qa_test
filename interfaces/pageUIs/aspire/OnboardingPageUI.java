package pageUIs.aspire;

public class OnboardingPageUI {
	public static final String CONTINUES_BUTTON = "//span[text() = 'Continue']";
	public static final String CONTINUES_ACRA_BUTTON = "//span[contains(text(),'Yes, my business is registered in Singapore with ACRA')]/parent::div/following-sibling::button//span[text()='Continue']";
	public static final String GET_STARTED_BUTTON = "//span[contains(text(),'Standard Registration')]/parent::div/following-sibling::button//span[text()='Get Started']";
}