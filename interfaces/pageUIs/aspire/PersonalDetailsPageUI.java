package pageUIs.aspire;

public class PersonalDetailsPageUI {
	public static final String GET_STARTED_BUTTON = "//span[text()='Get Started']";
	public static final String DATE_OF_BIRTH = "//input[@placeholder='Set your date of birth']";
	public static final String PARENT_NATIONAL = "//div[contains(text(),'Nationality')]/parent::div/following-sibling::label//i";
	public static final String CHILD_NATIONAL = "//div[@class='q-item__label']";
	public static final String PARENT_GENDER = "//div[contains(text(),'Gender')]/parent::div/following-sibling::label//i";
	public static final String CHILD_GENDER = "//div[@class='q-item__label']";
	public static final String PARENT_PRODUCTS = "//div[ @url='options']";
	public static final String CHILD_PRODUCTS = "//div[@class='q-checkbox__bg absolute']";
	public static final String SUBMIT_BUTTON = "//span[text()='Submit']";
}