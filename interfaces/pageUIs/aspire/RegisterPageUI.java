package pageUIs.aspire;

public class RegisterPageUI {
	public static final String REGISTER_BUTTON = "//a[contains(text(),'Register')]";
	public static final String DYNAMIC_TEXTBOX_NAME = "//input[@name='%s']";
	public static final String ROLE = "//div[text()='%s']";
	public static final String PARENT = "//input[@type='search']";
	public static final String CHILD = "//div[@class='q-item__label']";
	public static final String PROMODE_CODE = "//input[@placeholder='Enter the referral code or promo code']";
	public static final String TERMS_POLICY = "//div[@class='q-checkbox__bg absolute']";
	public static final String CONTINUE_BUTTON = "//span[text()='Continue']";
	public static final String OTP_NUMBER = "//div[@class='digit-input__box flex justify-between no-wrap']/div";
	
	
}