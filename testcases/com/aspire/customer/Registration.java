package com.aspire.customer;

import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import pageObject.aspire.BusinessDetailsPageObject;
import pageObject.aspire.MobileVerificationPageObject;
import pageObject.aspire.OnboardingPageObject;
import pageObject.aspire.PageGenaratorManager;
import pageObject.aspire.PersonalDetailsObject;
import pageObject.aspire.RegisterPageObject;

public class Registration extends AbstractTest {
	WebDriver driver;
	RegisterPageObject registerPage;
	MobileVerificationPageObject mobileVerificationPage;
	OnboardingPageObject onBoardingPage;
	PersonalDetailsObject personalDetailsPage;
	BusinessDetailsPageObject businessPage;
	String phonenumber = "0" + randomnumber();
	String email = "tester" + randomnumber() + "@gmail.com";

	@Parameters({ "browser", "url" })
	@BeforeClass
	public void beforeClass(String browserName, String url) {
		driver = getBrowserDriver(browserName, url);
		registerPage = PageGenaratorManager.getRegisterPage(driver);
	}

	@Test
	public void TC_01_Register_Valid_Data() {
		registerPage.clickToRegisterButton();
		registerPage.inputIntoTextboxName(driver, "full_name", "tester01");
		registerPage.inputIntoTextboxName(driver, "email", email);
		registerPage.inputIntoTextboxName(driver, "phone", phonenumber);
		registerPage.selectRole("Appointed director");
		registerPage.selectWhere("LinkedIn");
		registerPage.inputIntoPromoCode("1234");
		registerPage.agreeTermsAndConditions();
		registerPage.clickToContinueButton();
	}

//	@Test
//	public void TC_02_Mobile_Verification() {
//		for (int i = 0; i < 3; i++) {
//			mobileVerificationPage.inputOTPNumber("1");
//			mobileVerificationPage.inputOTPNumber("2");
//			mobileVerificationPage.inputOTPNumber("3");
//			mobileVerificationPage.inputOTPNumber("4");
//		}
//	}

	@Test
	public void TC_03_Personal_Details() {
		onBoardingPage = PageGenaratorManager.getOnboardingPage(driver);
		onBoardingPage.clickToContinuesButton();
		onBoardingPage.clickToContinuesButtonACRA();
		onBoardingPage.clickToGetStartedButton();
		personalDetailsPage = PageGenaratorManager.getPersonalDetailsPage(driver);
		personalDetailsPage.clickGetStartedButton();
		personalDetailsPage.sendKeyToDate();
		personalDetailsPage.selectNationality("Algeria");
		personalDetailsPage.selectGender("Female");
		personalDetailsPage.selectProduct("Debit Card");
		personalDetailsPage.clickToSubmitButton();

	}

//	@Test
//	public void TC_04_Email_Verification() {
//	for (int i = 0; i < 3; i++) {
//		mobileVerificationPage.inputOTPNumber("1");
//		mobileVerificationPage.inputOTPNumber("2");
//		mobileVerificationPage.inputOTPNumber("3");
//		mobileVerificationPage.inputOTPNumber("4");
//	}
//}

	@Test
	public void TC_05_Personal_Details() {
		businessPage = PageGenaratorManager.getBussinessDetailsPage(driver);
		businessPage.clickToGetStartedButton();
		businessPage.inputToBusinessName("BusinessName");
		businessPage.selectRegistrationType("Sole proprietorship");
		businessPage.inputUEN("12345678A");
		businessPage.selectIndustry("Retail Services");
		businessPage.selectSubIndustry("Automotive & Cars");
		businessPage.clickToSubmitButton();

	}

	public static int randomnumber() {
		Random rand = new Random();
		return rand.nextInt(999999999);
	}

	@AfterClass()
	public void afterClass() {
		driver.quit();
	}
}
